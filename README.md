Git global setup
git config --global user.name "Jose  Rojano"
git config --global user.email "rojanojose@outlook.com"
Create a new repository
git clone https://gitlab.com/u20-04/tus-servicios-medicos.git<br>
cd tus-servicios-medicos<br>
git switch -c main<br>
touch README.md<br>
git add README.md<br>
git commit -m "add README"<br>
git push -u origin main
Push an existing folder
cd existing_folder<br>
git init --initial-branch=main<br>
git remote add origin https://gitlab.com/u20-04/tus-servicios-medicos.git<br>
git add .<br>
git commit -m "Initial commit"<br>
git push -u origin main<br>
Push an existing Git repository
cd existing_repo<br>
git remote rename origin old-origin<br>
git remote add origin https://gitlab.com/u20-04/tus-servicios-medicos.git<br>
git push -u origin --all<br>
git push -u origin --tags

Añadido por la consola...

